//
//  AppDelegate.h
//  Test-Objc
//
//  Created by Afriyandi Setiawan on 05/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

