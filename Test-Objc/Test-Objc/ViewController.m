//
//  ViewController.m
//  Test-Objc
//
//  Created by Afriyandi Setiawan on 05/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    PassThroughSwift* fromSwift = [PassThroughSwift new];
    [fromSwift printFromSwift];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
