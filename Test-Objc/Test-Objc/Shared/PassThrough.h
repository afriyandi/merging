//
//  PassThrough.h
//  Test-Objc
//
//  Created by Afriyandi Setiawan on 05/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PassThrough : NSObject

-(void)PrintFromObjC;

@end
