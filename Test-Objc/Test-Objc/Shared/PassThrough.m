//
//  PassThrough.m
//  Test-Objc
//
//  Created by Afriyandi Setiawan on 05/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

#import "PassThrough.h"

@implementation PassThrough

-(void)PrintFromObjC {
    NSLog(@"Hello From the Objective C side");
}

@end
