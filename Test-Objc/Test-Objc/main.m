//
//  main.m
//  Test-Objc
//
//  Created by Afriyandi Setiawan on 05/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
