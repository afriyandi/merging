//
//  PassThrough.swift
//  Test-Swift
//
//  Created by Afriyandi Setiawan on 06/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import Foundation

@objc public class PassThroughSwift: NSObject {
    
    public override init() {
        super.init()
    }
    
    @objc public func printFromSwift() {
        print("Hello From The Swift Side")
    }
}
